# Sewing block generator



## Getting started

If you don't have python already on your computer, install it. If you get an error about tkinter when running this file, run
sudo apt-get install python3-tk 
^Linux

brew install python-tk
^MacOS

## Add your measurements
Open the block you want to make (e.g., bodice.py) in a text editor. At the top you will see:
measurementsCm={
    [measurement]:[number],
...
}
Replace each of the numbers with your measurement in centimeters. (E.g., replace "Neck":34 with "Neck":yourMeasurement)

##Generate block
I used this tutorial: https://inthefolds.com/blog/2016/2/22/how-to-draft-a-bodice-block 

Then simply run the file. In Linux or MacOS, open the folder the file is saved in using Terminal and type python3 bodice.py (or whatever filename)

This will make a .xps file image with your block. Check that it looks OK; it may not. Take it up with the tutorial maker :).

##Print and use
There is a 5cm by 5cm square included in the pattern. You will need to adjust the size of the picture when you print it to be 5cm by 5cm. I did this in GIMP by measuring the width of the square then scaling the image up such that the square was 5x5. 

Print as actual size and check the square.

You will need to curve off the neck holes and armpit holes.
