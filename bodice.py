from turtle import *
import turtle as t
import math

measurementsCm={
    "Neck":34,
    "Shoulder":11.5,
    "Chest above boobs":28.8,
    "Side neck point to nipple":24.8,
    "Bust":76,
    "Rib cage":63,
    "Nipple to nipple":17,
    "Circumference of arm at top":24.8,
    "Wrist":14.5,
    "Sleeve length":57,
    "Armscye Depth":19.8,
    "Back width":31.4,
    "Waist":60,
    "Front side neck point to waist":41.8,
    "Back side neck pont to waist":41.8,
    "Hip":84,
    "Nape to waist":41.8,
    "Nape to bust":24.8,
    "Ease":4
}
measurementsCm

right=0
north=90
left=180
south=270
#right 0, north 90,
#  180 left, 270 south
measurements={}
measurementsCm["To C"]=1.5
measurementsCm["Add to Armscye"]=.5
measurementsCm["KG Add"]=.5
measurementsCm["Down to Q"]=15
measurementsCm["Left to Q"]=3
measurementsCm["R"]=1.5
measurementsCm["Front dart subtract"]=.6
measurementsCm["Front dart starter"]=7
measurementsCm["Sub by increments of"]=4
measurementsCm["Sub starting from"]=88
measurementsCm["From bsutpoint to U"]=1
measurementsCm["W to BA"]=2.5
measurementsCm["AA to BB"]=1.5
measurementsCm["Dart ease"]=2
measurementsCm['front dart']=1
measurementsCm["Printing ref"]=5

for k,v in measurementsCm.items():
    measurements[k] = v*15


speed(0)

'''
print(screensize())
w = .6*measurements["Bust"]
h = 1.1*measurements["Nape to waist"]
screensize(canvwidth=w,canvheight=h)
print(screensize())


def f(xx,y):
    goto(xx,y)
    print(posNow(''))

onscreenclick(fun=f)
listen()

Actual canvas size = -800,800 x by -350 to 350 y
'''

def turtleInstructions():
    write("A")
    setheading(270) #right 0, north 90, 180 left, 270 south
    penup()
    setposition(100,100)
    pendown()
    forward(100)
    position()

def w(letter):
    #xx=letter+" "+str(round(position()[0]))+","+str(round(position()[1]))
    #Print in centimeters
    #print(xx)
    #write(letter)
    return position()

#Start in upper left
penup()
goto(-800,340)

#Center back
c=w("C")
setheading(270)
pendown()
forward(measurements["To C"])
a=w("A")
forward(measurements['Nape to waist'])
b=w("B")

penup()
goto(a)
forward(measurements["Nape to bust"])
d=w("D")

setheading(0)
xx= .5*measurements["Bust"]+.5*measurements["Ease"]
forward(xx)
e=w("E")
pendown()
goto(e[0],b[1])
f=w("F")
goto(e[0],c[1])
g=w("G")
goto(c)
penup()

penup()
goto(a)
setheading(270)
forward(measurements["Armscye Depth"]+measurements["Add to Armscye"])
h=w("H")
goto(e[0],h[1])
i=w("I")

penup()
goto(c)
pendown()
setheading(right)
forward(measurements["Neck"]/5)
j=w("J")
goto(j[0],a[1])
goto(a)

penup()
goto(g)
pendown()
setheading(left)
xx=measurements["Neck"]/5
xx=xx-measurements["KG Add"]
forward(xx)
k=w("K")
setheading(south)
forward(measurements["Neck"]/5)
setheading(right)
forward(xx)
l=w("L")

penup()
goto(a)
setheading(south)
xx=measurements["Armscye Depth"]/5
nChange = xx-measurements["Add to Armscye"]
forward(nChange)
n=w("N")

goto(j)
pendown()
hypotenuse = measurements['Shoulder']+1.5
b2=hypotenuse**2 - nChange**2
angle = math.asin(nChange/hypotenuse)
angle=math.degrees(angle)
b3 = b2 **.5 #Distance from where line south of J and line east of N meet, to O
xx=b3+j[0]
y=n[1]
goto(xx,y)
o=w("O")

px= j[0]+.5*b3
py = (j[1]+n[1])/2
goto(px,py)
p=w("P")

penup()
goto(a)
setheading(south)
forward(measurements["Down to Q"])
setheading(right)
xx= position()[0]-p[0]
forward(xx*-1)
setheading(left)
forward(measurements["Left to Q"])
q=w("Q")

goto(p)
setheading(360-angle-4)
forward(measurements["R"])
rB=w("BShD")
pendown()

goto(q)
goto(p)

penup()
goto(e)
CFtoBP=measurements["Nipple to nipple"]/2
CFtoBP= CFtoBP+measurements["Add to Armscye"]
setheading(left)
forward(CFtoBP)
BustPoint=w("BustPoint")
goto(BustPoint[0],k[1])
r=w("R")
goto(r[0],b[1])
s=w("S")

penup()
goto(l)
setheading(north)
forward(measurements["Add to Armscye"])
loggedForFrontShoulder=position() 

goto(k)
subBy= measurements["Sub starting from"]-measurements["Bust"]
subBy2 = subBy/measurements["Sub by increments of"]
subBy3=subBy2*measurements["Front dart subtract"] 
dartwidth = measurements["Front dart starter"] - subBy3

setheading(left)
forward(dartwidth)
tSpot=w("T")

goto(BustPoint)
setheading(north)
forward(measurements["From bsutpoint to U"])
u=w("U")
pendown()
goto(k)
goto(tSpot)
goto(u)

goto(tSpot)

hypotenuse = measurements['Shoulder']+1.5
lChange = tSpot[1]-loggedForFrontShoulder[1] #Difference ebtween T's height and Looged For's height
b2=hypotenuse**2 - lChange**2
angle = math.asin(lChange/hypotenuse)
angle=math.degrees(angle)
b3 = b2 **.5 #Distance from where line south of J and line east of N meet, to O
xx=tSpot[0]-b3
y=loggedForFrontShoulder[1]
goto(xx,y)
v=w("V")

penup()
goto(h)
xx= measurements["Back width"]/2
xx= xx+ measurements["Add to Armscye"]
seth(right)
forward(xx)
wSpot=w("W")

goto(wSpot[0],n[1])
x=w("X")
seth(south)
xx=(position()[1]-wSpot[1])/2
forward(xx)
z=w("Z")

penup()
#calculate angle of TUR
##length of RU =
ru = r[1]-u[1]
tr = tSpot[0]-r[0]
##length of TU = (TR**2 + RU**2)**.5
tu=(tr**2+ru**2)**.5

#calculate length of U->HI
utoHI=i[1]-u[1]
ratio = utoHI/ru
sideChange=tr*ratio
#use to calculate length of HI between lines TU and RU

goto(u[0],i[1])
seth(left)
forward(sideChange*2)
forward(CFtoBP)
aa=w("AA")

seth(north)
forward(measurements["Armscye Depth"]/5)
ab=w("AB")

goto(aa)
seth(left)
forward((aa[0]-wSpot[0])/2)
ac=w("ac")
pendown()
goto(ac[0],b[1])
ad=w("AD")

penup()
goto(aa)
setheading(135)
forward(measurements["AA to BB"])
bb = w("BB")
dot()

goto(wSpot)
seth(45)
forward(measurements["W to BA"])
ba=w("BA")
dot()

goto(o)
pendown()
goto(z)
goto(ba)
goto(ac)
goto(bb)
goto(ab)
goto(v)
penup()

xx=(wSpot[0]-h[0])/2 + h[0]
goto(xx,h[1])
ca=w("CA")

dartwidth=(measurements["Bust"]-measurements["Waist"]-measurements["Dart ease"])/6

goto(ca[0],b[1])
cb=w("CB")
seth(right)
forward(.5*dartwidth)
cc=w("CC")
seth(left)
forward(dartwidth)
cd=w("CD")
pendown()
goto(ca)
goto(cc)

penup()
goto(ad)
seth(left)
forward(dartwidth/3)
ae=w("AE")
seth(right)
forward(dartwidth)
af=w("AF")
pendown()
goto(ac)
goto(ae)

penup()
goto(s)
seth(left)
forward(.5*dartwidth)
da=w("DA")
seth(right)
forward(dartwidth)
db=w("DB")
pendown()
goto(s[0],BustPoint[1]-measurements['front dart'])
goto(da)

penup()
goto(f)

pendown()
seth(south)
forward(measurements['front dart'])
goto(b)

penup()
goto(g)
seth(right)
forward(measurements["W to BA"])
pendown()
forward(measurements['Printing ref'])
seth(south)
forward(measurements['Printing ref'])
seth(left)
forward(measurements['Printing ref'])
seth(north)
forward(measurements['Printing ref'])
write("5cm x 5cm square")


#mainloop()

getscreen().getcanvas().postscript(file="bodice.eps")
